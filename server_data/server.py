import socket
import struct
import os
import time
import shutil

class client:
    def __init__(self, name, password, id_u, role = 'untrust', can_edit = False):
        self.name = name
        self.password = password
        self.id = id_u
        self.role = role
        self.can_edit = can_edit
    
    def __str__(self):
        res = ''
        res += 'Login: ' + self.name + '\n'
        res += 'ID: ' + str(self.id) + '\n'
        res += 'Role: ' + self.role + '\n'
        return res

    def usr_data_to_admin(self):
        res = ''
        res += 'Login: ' + self.name + '\n'
        res += 'Password: ' + self.password + '\n'
        res += 'ID: ' + str(self.id) + '\n'
        res += 'Role: ' + self.role + '\n'
        res += 'Can edit: ' + str(self.can_edit) + '\n'
        return res

    def load_data_to_file(self):
        res = self.name + '\t' + self.password + '\t' + str(self.id) + '\t' + self.role + '\t' + str(self.can_edit)
        return res

    def change_role(self, new_role):
        self.role = new_role

    def enable_to_edit(self):
        self.can_edit = True

CLIENTS = [] 

ip_addr = 'localhost'
port = 8080
pack_size = 16384

def correct_path(path):
    path.replace('\\', '/')
    return path

def find_login(login):
    for x in CLIENTS:
        if x.name == login:
            return True
    return False

def find_id(userid):
    for x in CLIENTS:
        if x.id == int(userid):
            return True
    return False

def check_user_data(data):
    if len(data) < 3:
        #"Поля <login>, <password>, <id_u> являются обязательными."
        return 0
    try:
        a = int(data[2])
        return 2
    except:
        return 1
    '''if type(data[2]) != type(0):
        #"Поле <id_u> должно быть целым числом"
        return 1
    return 2'''

def get_level_by_role(role):
    if role == 'high_admin':
        return 'top_secret'
    elif role == 'low_admin':
        return 'confidential'
    elif role == 'top':
        return 'top_secret'
    elif role == 'trust':
        return 'secret'
    elif role == 'untrust':
        return 'confidential'
    elif role == 'user':
        return 'unconfidential'

def find_article(role, level, filename, mode = 'r'):
    print('Производится поиск статьи')
    os.chdir('articles')
    path = 'articles'
    if role == 'high_admin':
        os.chdir(level)
        path += '/' + level
        try:
            FILE = open(filename, mode)
            FILE.close()
            path += '/' + filename
            os.chdir('..')
            os.chdir('..')
            return path
        except:
            os.chdir('..')
            os.chdir('..')
            return 'file is not found'
        
    elif role == 'low_admin':
        if level == 'top_secret' or level == 'secret' or level == 'confidential':
            print('Доступ запрещён!')
            os.chdir('..')
            return 'access denied'
        else:
            os.chdir(level)
            path += '/' + level
            try:
                FILE = open(filename, mode)
                FILE.close()
                path += '/' + filename
                os.chdir('..')
                os.chdir('..')
                return path
            except:
                os.chdir('..')
                os.chdir('..')
                return 'file is not found'
    elif role == 'top':
        os.chdir(level)
        path += '/' + level
        try:
            FILE = open(filename, mode)
            FILE.close()
            path += '/' + filename
            os.chdir('..')
            os.chdir('..')
            return path
        except:
            os.chdir('..')
            os.chdir('..')
            return 'file is not found'
    elif role == 'trust':
        if level == 'top_secret':
            print('Доступ запрещён!')
            os.chdir('..')
            return 'access denied'
        else:
            os.chdir(level)
            path += '/' + level
            try:
                FILE = open(filename, mode)
                FILE.close()
                path += '/' + filename
                os.chdir('..')
                os.chdir('..')
                return path
            except:
                os.chdir('..')
                os.chdir('..')
                return 'file is not found'
    elif role == 'untrust':
        if level == 'top_secret' or level == 'secret':
            print('Доступ запрещён!')
            os.chdir('..')
            return 'access denied'
        else:
            os.chdir(level)
            path += '/' + level
            try:
                FILE = open(filename, mode)
                FILE.close()
                path += '/' + filename
                os.chdir('..')
                os.chdir('..')
                return path
            except:
                os.chdir('..')
                os.chdir('..')
                return 'file is not found'
    elif role == 'user':
        if level == 'top_secret' or level == 'secret' or level == 'confidential':
            print('Доступ запрещён!')
            os.chdir('..')
            return 'access denied'
        else:
            os.chdir(level)
            path += '/' + level
            try:
                FILE = open(filename, mode)
                FILE.close()
                path += '/' + filename
                os.chdir('..')
                os.chdir('..')
                return path
            except:
                os.chdir('..')
                os.chdir('..')
                return 'file is not found'

def load_article(filename, level):
    #os.replace("renamed-text.txt", "folder/renamed-text.txt")
    path =  "article\\" + level + "\\"
    #os.replace(filename, path)
    source_dir = os.path.abspath(os.curdir)
    destination_dir = source_dir + "\\" + path
    print(source_dir, destination_dir)
    print(correct_path(destination_dir))
    #shutil.move(os.path.join(source_dir, filename), destination_dir)
    pass

def receive_file_size(sck: socket.socket):
    # Эта функция обеспечивает получение байтов, 
    # указывающих на размер отправляемого файла, 
    # который кодируется клиентом с помощью 
    # struct.pack(), функции, которая генерирует 
    # последовательность байтов, представляющих размер файла.
    fmt = "<Q"
    expected_bytes = struct.calcsize(fmt)
    received_bytes = 0
    stream = bytes()
    while received_bytes < expected_bytes:
        chunk = sck.recv(expected_bytes - received_bytes)
        stream += chunk
        received_bytes += len(chunk)
    filesize = struct.unpack(fmt, stream)[0]
    return filesize

def receive_file(sck: socket.socket, filename):
    # Сначала считываем из сокета количество 
    # байтов, которые будут получены из файла.
    filesize = receive_file_size(sck)
    # Открываем новый файл для сохранения
    # полученных данных.
    with open(filename, "wb") as f:
        received_bytes = 0
        # Получаем данные из файла блоками по
        # 1024 байта до объема
        # общего количество байт, сообщенных клиентом.
        while received_bytes < filesize:
            chunk = sck.recv(1024)
            if chunk:
                f.write(chunk)
                received_bytes += len(chunk)

def send_file(sck: socket.socket, filename):
    # Получение размера файла.
    filesize = os.path.getsize(filename)
    # В первую очередь сообщим серверу, 
    # сколько байт будет отправлено.
    sck.sendall(struct.pack("<Q", filesize))
    # Отправка файла блоками по 1024 байта.
    with open(filename, "rb") as f:
        while read_bytes := f.read(1024):
            sck.sendall(read_bytes)

def test_file_com():
    with socket.create_server((ip_addr, port)) as server:
        print("Ожидание клиента...")
        conn, address = server.accept()
        print(f"{address[0]}:{address[1]} подключен.")
        print("Получаем файл...")
        receive_file(conn, "image-received.png")
        print("Файл получен.")
    print("Соединение закрыто.")

def print_list_of_articles(role):
    res = ''
    if role == 'high_admin':
        '''print("Все папки и файлы:", os.listdir())
        for dirpath, dirnames, filenames in os.walk("."):
            # перебрать каталоги
            for dirname in dirnames:
                print("Каталог:", os.path.join(dirpath, dirname))
            # перебрать файлы
            for filename in filenames:
                print("Файл:", os.path.join(dirpath, filename))'''
    
        os.chdir('articles')
        os.chdir('topsecret')
        #print("Все файлы уровня top_secret:")
        res += "Все файлы уровня top_secret:\n"
        for i in range(len(os.listdir())):
            #print(f'{i + 1}) {os.listdir()[i]}')
            res += f'{i + 1}) {os.listdir()[i]}' + '\n'
        os.chdir('..')
        
        os.chdir('secret')
        #print("Все файлы уровня secret:")
        res += "Все файлы уровня secret:\n"
        for i in range(len(os.listdir())):
            #print(f'{i + 1}) {os.listdir()[i]}')
            res += f'{i + 1}) {os.listdir()[i]}' + '\n'
        os.chdir('..')

        os.chdir('confidential')
        #print("Все файлы уровня confidential:")
        res += "Все файлы уровня confidential:\n"
        for i in range(len(os.listdir())):
            #print(f'{i + 1}) {os.listdir()[i]}')
            res += f'{i + 1}) {os.listdir()[i]}' + '\n'
        os.chdir('..')

        os.chdir('unconfidential')
        #print("Все файлы уровня unconfiedential:")
        res += "Все файлы уровня unconfiedential:\n"
        for i in range(len(os.listdir())):
            #print(f'{i + 1}) {os.listdir()[i]}')
            res += f'{i + 1}) {os.listdir()[i]}' + '\n'
        os.chdir('..')
        os.chdir('..')
        #print(os.getcwd())

    elif role == 'low_admin':
        os.chdir('articles')
        '''os.chdir('secret')
        #print("Все файлы уровня secret:")
        res += "Все файлы уровня secret:\n"
        for i in range(len(os.listdir())):
            #print(f'{i + 1}) {os.listdir()[i]}')
            res += f'{i + 1}) {os.listdir()[i]}' + '\n'
        os.chdir('..')'''

        os.chdir('confidential')
        #print("Все файлы уровня confidential:")
        res += "Все файлы уровня confidential:\n"
        for i in range(len(os.listdir())):
            #print(f'{i + 1}) {os.listdir()[i]}')
            res += f'{i + 1}) {os.listdir()[i]}' + '\n'
        os.chdir('..')

        os.chdir('unconfidential')
        #print("Все файлы уровня unconfiedential:")
        res += "Все файлы уровня unconfiedential:\n"
        for i in range(len(os.listdir())):
            #print(f'{i + 1}) {os.listdir()[i]}')
            res += f'{i + 1}) {os.listdir()[i]}' + '\n'
        os.chdir('..')
        os.chdir('..')
        #print(os.getcwd())

    elif role == 'top':
        os.chdir('articles')
        os.chdir('topsecret')
        i = 0
        #print("Все файлы уровня top_secret:")
        res +="Все файлы уровня top_secret:" + '\n'
        for i in range(len(os.listdir())):
            #print(f'{i + 1}) {os.listdir()[i]}')
            res += f'{i + 1}) {os.listdir()[i]}' + '\n'
        os.chdir('..')
        
        os.chdir('secret')
        #print("Все файлы уровня secret:")
        res += "Все файлы уровня secret:" + '\n'
        for i in range(len(os.listdir())):
            #print(f'{i + 1}) {os.listdir()[i]}')
            res += f'{i + 1}) {os.listdir()[i]}' + '\n'
        os.chdir('..')

        os.chdir('confidential')
        #print("Все файлы уровня confidential:")
        res += "Все файлы уровня confidential:\n"
        for i in range(len(os.listdir())):
            #print(f'{i + 1}) {os.listdir()[i]}')
            res += f'{i + 1}) {os.listdir()[i]}' + '\n'
        os.chdir('..')

        os.chdir('unconfidential')
        #print("Все файлы уровня unconfiedential:")
        res += "Все файлы уровня unconfiedential:\n"
        for i in range(len(os.listdir())):
            #print(f'{i + 1}) {os.listdir()[i]}')
            res += f'{i + 1}) {os.listdir()[i]}' + '\n'
        os.chdir('..')
        os.chdir('..')
        #print(os.getcwd())
        
    elif role == 'trust':
        os.chdir('articles')
        os.chdir('secret')
        #print("Все файлы уровня secret:")
        res += "Все файлы уровня secret:\n"
        for i in range(len(os.listdir())):
            #print(f'{i + 1}) {os.listdir()[i]}')
            res += f'{i + 1}) {os.listdir()[i]}' + '\n'
        os.chdir('..')

        os.chdir('confidential')
        #print("Все файлы уровня confidential:")
        res += "Все файлы уровня confidential:\n"
        for i in range(len(os.listdir())):
            #print(f'{i + 1}) {os.listdir()[i]}')
            res += f'{i + 1}) {os.listdir()[i]}' + '\n'
        os.chdir('..')

        os.chdir('unconfidential')
        #print("Все файлы уровня unconfiedential:")
        res += "Все файлы уровня unconfiedential:\n"
        for i in range(len(os.listdir())):
            #print(f'{i + 1}) {os.listdir()[i]}')
            res += f'{i + 1}) {os.listdir()[i]}' + '\n'
        os.chdir('..')
        os.chdir('..')
        #print(os.getcwd())
        
    elif role == 'untrust':
        os.chdir('articles')
        os.chdir('confidential')
        #print("Все файлы уровня confidential:")
        res += "Все файлы уровня confidential:\n"
        for i in range(len(os.listdir())):
            #print(f'{i + 1}) {os.listdir()[i]}')
            res += f'{i + 1}) {os.listdir()[i]}' + '\n'
        os.chdir('..')

        os.chdir('unconfidential')
        #print("Все файлы уровня unconfiedential:")
        res += "Все файлы уровня unconfiedential:\n"
        for i in range(len(os.listdir())):
            #print(f'{i + 1}) {os.listdir()[i]}')
            res += f'{i + 1}) {os.listdir()[i]}' + '\n'
        os.chdir('..')
        os.chdir('..')
        #print(os.getcwd())
        
    elif role == 'user':
        os.chdir('articles')
        os.chdir('unconfidential')
        #print("Все файлы уровня unconfiedential:")
        res += "Все файлы уровня unconfiedential:\n"
        for i in range(len(os.listdir())):
            #print(f'{i + 1}) {os.listdir()[i]}')
            res += f'{i + 1}) {os.listdir()[i]}' + '\n'
        os.chdir('..')
        os.chdir('..')
        #print(os.getcwd())
    return res

def remove_files(directory):
    os.chdir(directory)
    for i in range(0, len(os.listdir())):
        cur_dir = os.listdir()[i]
        os.chdir(cur_dir)
        print(cur_dir)
        for file in os.scandir(cur_dir):
            print(file.path)
            os.remove(file.path)
    
        os.chdir('..')
    os.chdir('..')


def main():
    try:
        FILE = open('data_for_server/usersdata.txt', 'r')
    except:
        print("Not file with users!")
        return
    for x in FILE:
        usrdat = x.split('\t')
        CLIENTS.append(client(usrdat[0], usrdat[1], int(usrdat[2]), usrdat[3], bool(usrdat[4])))
    FILE.close()
    for x in CLIENTS:
        print(x)
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
    server_socket.bind((ip_addr, port))
    server_socket.listen(0)
    print("SERVER STARTS!!!")
    client_role = 'user'
    while True:
        conn, address = server_socket.accept()
        message_to_client = "Здравстуйте! Вас приветсвует бюро общественной безпасности! Пожалуйста, введите информацию, касающуюся цели вашего визита\n1 - войти в аккаунт\n2 - ознакомиться с общедоступной информацией"
        print('ITER')
        conn.send(message_to_client.encode('utf-8'))
        message = ((conn.recv(1024)).decode('utf-8'))
        print(message)
        if (message == '1'):
            access = False
            attempt = 0
            message_to_client = 'Пожалуйста, введите данные для входа в аккаунт!'
            while attempt != 3:
                conn.send(message_to_client.encode('utf-8'))
                login = ((conn.recv(1024)).decode('utf-8'))
                password = ((conn.recv(1024)).decode('utf-8'))
                print(login, password, attempt)
                for x in CLIENTS:
                    if x.name == login and x.password == password:
                        access = True
                        client_role = x.role
                        can_edit = x.can_edit
                        break
                if access == True:
                    message_to_client = "Вы были успешно авторизованы! Можете продолжать пользоваться сайтом"
                    conn.send(message_to_client.encode('utf-8'))
                    break
                else:
                    message_to_client = "Логин или пароль неверны! Попробуйте ввести из ещё раз"
                    conn.send(message_to_client.encode('utf-8'))
                    attempt += 1
            if access == True:
                print('dostup')
                message_to_client = f'Ваша роль - {client_role}. Выберите опцию:\n'
                conn.send(message_to_client.encode('utf-8'))
                time.sleep(1)
                print('dostup1')
                if client_role == 'high_admin':
                    while True:
                        print('dostup2')
                        message_to_client = '''Что вы хотите сделать(введите соответствующий номер)\n
                        1 - Создать(добавить) пользователя\n
                        2 - Удалить пользователя\n
                        3 - удалить базу данных с пользователями\n
                        4 - просмотреть список всех пользователей\n
                        5 - добавить статью/файл\n
                        6 - получить статью\n
                        7 - читать статью\n
                        8 - редактировать статью\n
                        9 - остановить работу сервера\n
                        10 - прочитать файл с логами\n
                        11 - прочитать файл с кaндидатами\n
                        12 - очистить базу данных со статьями\n
                        13 - просмотреть список статей\n
                        14 - выйти\n
                        '''
                        conn.send(message_to_client.encode('utf-8'))
                        message = ((conn.recv(1024)).decode('utf-8'))
                        if message == '1':
                            #Создать(добавить) пользователя
                            message_to_client = 'Введите данные пользователя(login, password, id_u, role, can_edit): '
                            conn.send(message_to_client.encode('utf-8'))
                            message = ((conn.recv(1024)).decode('utf-8'))
                            usrdat = message.split(' ')
                            if find_id(usrdat[2]) == True or find_login(usrdat[0]) == True:
                                message_to_client = 'Введённый логин или id уже используются!'
                                conn.send(message_to_client.encode('utf-8'))
                            elif check_user_data(usrdat) == 0:
                                message_to_client = 'Поля <login>, <password>, <id_u> являются обязательными.'
                                conn.send(message_to_client.encode('utf-8'))
                            elif check_user_data(usrdat) == 1:
                                message_to_client = 'Поле <id_u> должно быть целым числом.'
                                conn.send(message_to_client.encode('utf-8'))
                            else:
                                CLIENTS.append(client(usrdat[0], usrdat[1], usrdat[2], usrdat[3], usrdat[4]))
                                FILE = open('data_for_server/usersdata.txt', 'a+')
                                string = str(usrdat[0] + '\t' + usrdat[1] + '\t' + usrdat[2] + '\t' + usrdat[3] + '\t' + usrdat[4])
                                FILE.write(string + '\n')
                                FILE.close()
                                message_to_client = 'Данные пользователя успешно введены'
                                conn.send(message_to_client.encode('utf-8'))
                            pass
                        elif message == '2':
                            #Удалить пользователя
                            message_to_client = 'Введите данные пользователя, которого хотите удалить(login): '
                            conn.send(message_to_client.encode('utf-8'))
                            message = ((conn.recv(1024)).decode('utf-8'))
                            flag = False
                            for x in CLIENTS:
                                if x.name == message:
                                    CLIENTS.remove(x)
                                    flag = True
                                    FILE = open('data_for_server/usersdata.txt', 'w')
                                    for y in CLIENTS:
                                        FILE.write(y.load_data_to_file() + '\n')
                                    FILE.close()
                                    break
                            if flag == True:
                                message_to_client = f'Пользователь с логином {message} был удалён.'
                                conn.send(message_to_client.encode('utf-8'))
                            else:
                                message_to_client = f'Пользователя с логином {message} нет в системе базы данных.'
                                conn.send(message_to_client.encode('utf-8'))
                            pass
                        elif message == '3':
                            #удалить базу данных с пользователями
                            message_to_client = 'Вы действительно хотите удалить базу данных с пользователями?: '
                            conn.send(message_to_client.encode('utf-8'))
                            message = ((conn.recv(1024)).decode('utf-8'))
                            if message == 'да' or message == 'ДА' or message == 'yes' or message == 'YES':
                                os.chdir('data_for_server')
                                FILE = open('users_data.txt', 'w')
                                FILE.close()
                            message_to_client = 'База данных с пользователями удалена.'
                            conn.send(message_to_client.encode('utf-8'))
                            pass
                        elif message == '4':
                            #просмотреть список всех пользователей
                            message_to_client = ''
                            for x in CLIENTS:
                                #print(x.usr_data_to_admin())
                                message_to_client += x.usr_data_to_admin()
                            conn.send(message_to_client.encode('utf-8'))
                            pass
                        elif message == '5':
                            #добавить статью/файл
                            message_to_client = 'Ожидаю файл(Введите название файла и его уровен): '
                            conn.send(message_to_client.encode('utf-8'))
                            message = ((conn.recv(1024)).decode('utf-8'))
                            print(message)
                            if message != "Notfound":
                                message = message.split(' ')
                                receive_file(conn, message[0])
                                print(message[1])
                                os.replace(message[0], str('articles/' + message[1] + '/' + message[0]))
                            pass
                        elif message == '6':
                            #получить статью
                            message_to_client = 'Название статьи, которую хотите получить и её уровень: '
                            conn.send(message_to_client.encode('utf-8'))
                            message = ((conn.recv(1024)).decode('utf-8'))
                            message = message.split(' ')
                            path = find_article(client_role, message[1], message[0])
                            if path == 'access denied':
                                message_to_client = 'У вас нет соотвтетсвующих прав доступа'
                                conn.send(message_to_client.encode('utf-8'))
                            elif path == 'file is not found':
                                message_to_client = 'Данный файл не найден. Будьте внимательны и повторите попытку'
                                conn.send(message_to_client.encode('utf-8'))
                            else:
                                path = message[1]
                                f_name = message[0]
                                message_to_client = 'Разрешён доступ'
                                conn.send(message_to_client.encode('utf-8'))
                                send_file(conn, 'articles/' + path + '/' + f_name)
                            pass
                        elif message == '7':
                            #читать статью
                            message_to_client = 'Данная функция пока недоступна'
                            conn.send(message_to_client.encode('utf-8'))
                            pass
                        elif message == '8':
                            #редактировать статью
                            if can_edit == False:
                                message_to_client = 'Вы не можете редактировать!'
                                conn.send(message_to_client.encode('utf-8'))
                            else:
                                message_to_client = 'Название статьи, которую хотите отредактировать и её уровень: '
                                conn.send(message_to_client.encode('utf-8'))
                                message = ((conn.recv(1024)).decode('utf-8'))
                                message = message.split(' ')
                                path = find_article(client_role, message[1], message[0])
                                if path == 'access denied':
                                    message_to_client = 'У вас нет соотвтетсвующих прав доступа'
                                    conn.send(message_to_client.encode('utf-8'))
                                elif path == 'file is not found':
                                    message_to_client = 'Данный файл не найден. Будьте внимательны и повторите попытку'
                                    conn.send(message_to_client.encode('utf-8'))
                                else:
                                    path = message[1]
                                    f_name = message[0]
                                    message_to_client = 'Разрешён доступ'
                                    conn.send(message_to_client.encode('utf-8'))
                                    send_file(conn, 'articles/' + path + '/' + f_name)
                                    #some = ''
                                    receive_file(conn, f_name)
                                    #if some != f_name:
                                    #    os.rename(some, f_name)
                                    os.replace(f_name, str('articles/' + path + '/' + f_name))
                            pass
                        elif message == '9':
                            #остановить работу сервера
                            message_to_client = 'Вы действительно хотите остановить сервер?'
                            conn.send(message_to_client.encode('utf-8'))
                            message = ((conn.recv(1024)).decode('utf-8'))
                            if message == 'да' or message == 'ДА' or message == 'yes' or message == 'YES':
                                print("OK! Я отключаюсь")
                                message_to_client = 'OFF!!!'
                                conn.send(message_to_client.encode('utf-8'))
                                conn.close()
                                exit()
                            else:
                                print('continue')
                                message_to_client = 'WORK!!!'
                                conn.send(message_to_client.encode('utf-8'))
                            pass
                        elif message == '10':
                            #прочитать файл с логами
                            message_to_client = 'Логи:\n'
                            conn.send(message_to_client.encode('utf-8'))
                            #message = ((conn.recv(1024)).decode('utf-8'))
                            os.chdir('data_for_server')
                            message_to_client = ''
                            FILE = open('logs.txt', 'r')
                            for x in FILE:
                                message_to_client += x
                            message_to_client += '\n'
                            conn.send(message_to_client.encode('utf-8'))
                            FILE.close()
                            os.chdir('..')
                            pass
                        elif message == '11':
                            #прочитать файл с кaндидатами
                            message_to_client = 'Кандидаты на регистрацию:\n'
                            conn.send(message_to_client.encode('utf-8'))
                            os.chdir('data_for_server')
                            message_to_client = ''
                            FILE = open('user_to_registrate.txt', 'r')
                            for x in FILE:
                                message_to_client += x
                            message_to_client += '\n'
                            conn.send(message_to_client.encode('utf-8'))
                            FILE.close()
                            os.chdir('..')
                            pass
                        elif message == '12':
                            #очистить базу данных со статьями
                            remove_files('articles')
                            message_to_client = 'База данных очищена: '
                            conn.send(message_to_client.encode('utf-8'))
                        elif message == '13':
                            #просмотреть список статей
                            message_to_client = print_list_of_articles(client_role)
                            conn.send(message_to_client.encode('utf-8'))
                        elif message == '14':
                            #остановить
                            break
                        else:
                            message_to_client = f'Вы ввели некорректный номер. Попробуйте ещё раз.'
                            conn.send(message_to_client.encode('utf-8'))
                elif client_role == 'low_admin':
                    while True:
                        print('dostup2')
                        message_to_client = '''Что вы хотите сделать(введите соответствующий номер)\n
                        1 - Создать(добавить) пользователя\n
                        2 - Удалить пользователя\n
                        3 - добавить статью/файл\n
                        4 - редактировать статью\n
                        5 - получить статью\n
                        6 - просмотреть список статей\n
                        7 - выйти\n'''
                        conn.send(message_to_client.encode('utf-8'))
                        message = ((conn.recv(1024)).decode('utf-8'))
                        if message == '1':
                            message_to_client = 'Введите данные пользователя(login, password, id_u, role, can_edit): '
                            conn.send(message_to_client.encode('utf-8'))
                            message = ((conn.recv(1024)).decode('utf-8'))
                            usrdat = message.split(' ')
                            print(usrdat)
                            if find_id(usrdat[2]) == True or find_login(usrdat[0]) == True:
                                message_to_client = 'Введённый логин или id уже используются!'
                                conn.send(message_to_client.encode('utf-8'))
                            elif check_user_data(usrdat) == 0:
                                message_to_client = 'Поля <login>, <password>, <id_u> являются обязательными.'
                                conn.send(message_to_client.encode('utf-8'))
                            elif check_user_data(usrdat) == 1:
                                message_to_client = 'Поле <id_u> должно быть целым числом.'
                                conn.send(message_to_client.encode('utf-8'))
                            elif usrdat[3] == 'top' or usrdat[3] == 'trust':
                                message_to_client = 'У вас нет доступа для создания пользователя с такими привилегиями'
                                conn.send(message_to_client.encode('utf-8')) 
                            else:
                                print('Заполняю!')
                                CLIENTS.append(client(usrdat[0], usrdat[1], usrdat[2], usrdat[3], usrdat[4]))
                                FILE = open('data_for_server/usersdata.txt', 'a+')
                                string = str(usrdat[0] + '\t' + usrdat[1] + '\t' + usrdat[2] + '\t' + usrdat[3] + '\t' + usrdat[4])
                                FILE.write(string + '\n')
                                FILE.close()
                                message_to_client = 'Данные пользователя успешно введены'
                                conn.send(message_to_client.encode('utf-8'))
                            pass
                        elif message == '2':
                            message_to_client = 'Введите данные пользователя, которого хотите удалить(login): '
                            conn.send(message_to_client.encode('utf-8'))
                            message = ((conn.recv(1024)).decode('utf-8'))
                            flag = 0
                            for x in CLIENTS:
                                if x.name == message:
                                    if x.role != 'untrust':
                                        flag = 1
                                        break
                                    CLIENTS.remove(x)
                                    flag = 2
                                    FILE = open('data_for_server/usersdata.txt', 'w')
                                    for y in CLIENTS:
                                        FILE.write(y.load_data_to_file() + '\n')
                                    FILE.close()
                                    break
                            if flag == 2:
                                message_to_client = f'Пользователь с логином {message} был удалён.'
                                conn.send(message_to_client.encode('utf-8'))
                            elif flag == 1:
                                message_to_client = f'Пользователь с логином {message} не может быть удалён, так как у вас нет доступа на это право.'
                                conn.send(message_to_client.encode('utf-8'))
                            else:
                                message_to_client = f'Пользователя с логином {message} нет в системе базы данных.'
                                conn.send(message_to_client.encode('utf-8'))
                            pass
                        elif message == '3':
                            #Добавить статью
                            message_to_client = 'Ожидаю файл(Введите название файла и его уровень(uncofidential, confidential, secret, topsecret): '
                            conn.send(message_to_client.encode('utf-8'))
                            message = ((conn.recv(1024)).decode('utf-8'))
                            print(message)
                            if message != "Notfound":
                                message = message.split(' ')
                                if message[1] == 'secret' or message[1] == 'topsecret':
                                    message_to_client = 'Not access'
                                    conn.send(message_to_client.encode('utf-8'))
                                else:
                                    message_to_client = 'Access'
                                    conn.send(message_to_client.encode('utf-8'))
                                    receive_file(conn, message[0])
                                    print(message[1])
                                    os.replace(message[0], str('articles/' + message[1] + '/' + message[0]))
                        elif message == '4':
                            #Редактировать статью
                            if can_edit == False:
                                message_to_client = 'Вы не можете редактировать!'
                                conn.send(message_to_client.encode('utf-8'))
                            else:
                                message_to_client = 'Название статьи, которую хотите отредактировать и её уровень: '
                                conn.send(message_to_client.encode('utf-8'))
                                message = ((conn.recv(1024)).decode('utf-8'))
                                message = message.split(' ')
                                path = find_article(client_role, message[1], message[0])
                                if path == 'access denied':
                                    message_to_client = 'У вас нет соотвтетсвующих прав доступа'
                                    conn.send(message_to_client.encode('utf-8'))
                                elif path == 'file is not found':
                                    message_to_client = 'Данный файл не найден. Будьте внимательны и повторите попытку'
                                    conn.send(message_to_client.encode('utf-8'))
                                else:
                                    path = message[1]
                                    f_name = message[0]
                                    message_to_client = 'Разрешён доступ'
                                    conn.send(message_to_client.encode('utf-8'))
                                    send_file(conn, 'articles/' + path + '/' + f_name)
                                    #some = ''
                                    receive_file(conn, f_name)
                                    #if some != f_name:
                                    #    os.rename(some, f_name)
                                    os.replace(f_name, str('articles/' + path + '/' + f_name))
                        elif message == '5':
                            message_to_client = 'Название статьи, которую хотите получить и её уровень: '
                            conn.send(message_to_client.encode('utf-8'))
                            message = ((conn.recv(1024)).decode('utf-8'))
                            message = message.split(' ')
                            path = find_article(client_role, message[1], message[0])
                            if path == 'access denied':
                                message_to_client = 'У вас нет соотвтетсвующих прав доступа'
                                conn.send(message_to_client.encode('utf-8'))
                            elif path == 'file is not found':
                                message_to_client = 'Данный файл не найден. Будьте внимательны и повторите попытку'
                                conn.send(message_to_client.encode('utf-8'))
                            else:
                                path = message[1]
                                f_name = message[0]
                                message_to_client = 'Разрешён доступ'
                                conn.send(message_to_client.encode('utf-8'))
                                send_file(conn, 'articles/' + path + '/' + f_name)
                        elif message == '6':
                            message_to_client = print_list_of_articles(client_role)
                            conn.send(message_to_client.encode('utf-8'))
                        elif message == '7':
                            #остановить
                            break
                        else:
                            message_to_client = f'Вы ввели некорректный номер. Попробуйте ещё раз.'
                            conn.send(message_to_client.encode('utf-8'))
                elif client_role == 'top' or client_role == 'trust' or client_role == 'untrust':
                    while True:
                        message_to_client += '''Что вы хотите сделать(введите соответствующий номер)\n
                        1 - добавить статью/файл\n
                        2 - редактировать статью\n
                        3 - получить статью/файл\n
                        4 - читать статью\n
                        5 - просмотреть список статей\n
                        6 - выйти\n'''
                        conn.send(message_to_client.encode('utf-8'))
                        message = ((conn.recv(1024)).decode('utf-8'))
                        #добавить статью
                        if message == '1':
                            message_to_client = 'Ожидаю файл(Введите название файла и его уровен): '
                            conn.send(message_to_client.encode('utf-8'))
                            message = ((conn.recv(1024)).decode('utf-8'))
                            print(message)
                            if message != "Notfound":
                                message = message.split(' ')
                                if client_role == 'top':
                                    message_to_client = 'Access'
                                    conn.send(message_to_client.encode('utf-8'))
                                    receive_file(conn, message[0])
                                    print(message[1])
                                    os.replace(message[0], str('articles/' + message[1] + '/' + message[0]))
                                elif client_role == 'trust':
                                    if message[1] == 'topsecret':
                                        message_to_client = 'Not access'
                                        conn.send(message_to_client.encode('utf-8'))
                                    else:
                                        message_to_client = 'Access'
                                        conn.send(message_to_client.encode('utf-8'))
                                        receive_file(conn, message[0])
                                        print(message[1])
                                        os.replace(message[0], str('articles/' + message[1] + '/' + message[0]))
                                elif client_role == 'untrust':
                                    if message[1] == 'secret' or message[1] == 'topsecret':
                                        message_to_client = 'Not access'
                                        conn.send(message_to_client.encode('utf-8'))
                                    else:
                                        message_to_client = 'Access'
                                        conn.send(message_to_client.encode('utf-8'))
                                        receive_file(conn, message[0])
                                        print(message[1])
                                        os.replace(message[0], str('articles/' + message[1] + '/' + message[0]))
                        #редактировать статью
                        elif message == '2':
                            if can_edit == False:
                                message_to_client = 'Вы не можете редактировать!'
                                conn.send(message_to_client.encode('utf-8'))
                            else:
                                message_to_client = 'Название статьи, которую хотите отредактировать и её уровень: '
                                conn.send(message_to_client.encode('utf-8'))
                                message = ((conn.recv(1024)).decode('utf-8'))
                                message = message.split(' ')
                                path = find_article(client_role, message[1], message[0])
                                if path == 'access denied':
                                    message_to_client = 'У вас нет соотвтетсвующих прав доступа'
                                    conn.send(message_to_client.encode('utf-8'))
                                elif path == 'file is not found':
                                    message_to_client = 'Данный файл не найден. Будьте внимательны и повторите попытку'
                                    conn.send(message_to_client.encode('utf-8'))
                                else:
                                    path = message[1]
                                    f_name = message[0]
                                    message_to_client = 'Разрешён доступ'
                                    conn.send(message_to_client.encode('utf-8'))
                                    send_file(conn, 'articles/' + path + '/' + f_name)
                                    #some = ''
                                    receive_file(conn, f_name)
                                    #if some != f_name:
                                    #    os.rename(some, f_name)
                                    os.replace(f_name, str('articles/' + path + '/' + f_name))
                        #получить статью
                        elif message == '3':
                            message_to_client = 'Название статьи, которую хотите получить и её уровень: '
                            conn.send(message_to_client.encode('utf-8'))
                            message = ((conn.recv(1024)).decode('utf-8'))
                            message = message.split(' ')
                            path = find_article(client_role, message[1], message[0])
                            if path == 'access denied':
                                message_to_client = 'У вас нет соотвтетсвующих прав доступа'
                                conn.send(message_to_client.encode('utf-8'))
                            elif path == 'file is not found':
                                message_to_client = 'Данный файл не найден. Будьте внимательны и повторите попытку'
                                conn.send(message_to_client.encode('utf-8'))
                            else:
                                path = message[1]
                                f_name = message[0]
                                message_to_client = 'Разрешён доступ'
                                conn.send(message_to_client.encode('utf-8'))
                                send_file(conn, 'articles/' + path + '/' + f_name)
                        #читать статью
                        elif message == '4':
                            message_to_client = 'Данная функция пока недоступна'
                            conn.send(message_to_client.encode('utf-8'))
                            pass
                        elif message == '5':
                            message_to_client = print_list_of_articles(client_role)
                            conn.send(message_to_client.encode('utf-8'))
                        elif message == '6':
                            #остановить
                            break
                        else:
                            message_to_client = f'Вы ввели некорректный номер. Попробуйте ещё раз.'
                            conn.send(message_to_client.encode('utf-8'))
                #conn.send(message_to_client.encode('utf-8'))
            else:
                message_to_client = 'Вы поход забыли пароль! До свиданья!'
                conn.send(message_to_client.encode('utf-8'))
        elif message == '2':
            print('GERE')
            while True:
                conn.send('GDE&&&'.encode('utf-8'))
                message_to_client = 'Вам присвоена роль обычного пользователя. Поэтому вы имеет доступ только к общедоступной информации!'
                message_to_client += '\nВыберите опцию:'
                message_to_client += '\n1 - Читать статью\n2 - Получить файл\n3 - Отправить запрос на регистрацию\n4 - Просмотреть список файлов\n5 - Отсоединиться'
                conn.send(message_to_client.encode('utf-8'))
                print('Я отправил')
                message = ((conn.recv(1024)).decode('utf-8'))
                print(message)
                if message == '1':
                    '''message_to_client = print_list_of_articles(role = 'user')
                    conn.send(message_to_client.encode('utf-8'))
                    message = ((conn.recv(1024)).decode('utf-8'))
                    message_to_client = ''
                    symbols = 0
                    try:
                        FILE = open(message, 'r')
                        for line in FILE:
                            symbols += len(line.strip('\n'))
                            if symbols >= pack_size:
                                conn.send(message_to_client.encode('utf-8'))
                                message_to_client = ''
                                message_to_client += line
                            else:
                                message_to_client += line

                        FILE.close()
                    except:
                        conn.send(message_to_client.encode('utf-8'))
                        message_to_client = line
                    '''
                    pass
                elif message == '2':
                    message_to_client = print_list_of_articles(role = 'user')
                    conn.send(message_to_client.encode('utf-8'))
                    message = ((conn.recv(1024)).decode('utf-8'))
                    print(message)
                    try:
                        message_to_client = 'Файл отправлен'
                        conn.send(message_to_client.encode('utf-8'))
                        send_file(conn, 'articles/unconfidential/' +  message)
                    except:
                        message_to_client = 'Не могу отправить файл'
                        conn.send(message_to_client.encode('utf-8'))
                    pass
                elif message == '3':
                    message_to_client = 'Введите свои данные(login, password, role): '
                    conn.send(message_to_client.encode('utf-8'))
                    message = ((conn.recv(1024)).decode('utf-8'))
                    usrdat = message.split(' ')
                    FILE = open('data_for_server/user_to_registrate.txt', 'a')
                    string = str(usrdat[0] + '\t' + usrdat[1] + '\t' + usrdat[2])
                    FILE.write('\n' + string)
                    FILE.close()
                    message_to_client = 'Ваши данные занесены в список на одобрение'
                    conn.send(message_to_client.encode('utf-8'))
                    pass
                elif message == '4':
                    message_to_client = print_list_of_articles(client_role)
                    conn.send(message_to_client.encode('utf-8'))
                elif message == '5':
                    break
                else:
                    message_to_client = f'Вы ввели некорректный номер. Попробуйте ещё раз.'
                    conn.send(message_to_client.encode('utf-8'))
        else:
            message_to_client = "Вы ввели неверную опцию! Попробуйте переподключиться."
            conn.send(message_to_client.encode('utf-8'))
            #message = ((conn.recv(1024)).decode('utf-8'))
        print('The next!')
        #break

if __name__ == '__main__':
    #test_file_com()
    main()
    #load_article('flop1.txt', 'confidential')
    #os.replace("flop2.txt", "articles/confidential/flop1.txt")
    #remove_files('articles')
    cl = client('add', '1234', 34, 'untrust', True)
    print(cl.load_data_to_file())
    print_list_of_articles('high_admin')
    print_list_of_articles('low_admin')
    print_list_of_articles('top')
    print_list_of_articles('trust')
    print_list_of_articles('untrust')
    print_list_of_articles('user')
    print('OK')