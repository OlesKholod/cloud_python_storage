import os
import socket
import struct
import time

ip_addr = 'localhost'
port = 8080

def receive_file_size(sck: socket.socket):
    # Эта функция обеспечивает получение байтов, 
    # указывающих на размер отправляемого файла, 
    # который кодируется клиентом с помощью 
    # struct.pack(), функции, которая генерирует 
    # последовательность байтов, представляющих размер файла.
    fmt = "<Q"
    expected_bytes = struct.calcsize(fmt)
    received_bytes = 0
    stream = bytes()
    while received_bytes < expected_bytes:
        chunk = sck.recv(expected_bytes - received_bytes)
        stream += chunk
        received_bytes += len(chunk)
    filesize = struct.unpack(fmt, stream)[0]
    return filesize

def receive_file(sck: socket.socket, filename):
    # Сначала считываем из сокета количество 
    # байтов, которые будут получены из файла.
    filesize = receive_file_size(sck)
    # Открываем новый файл для сохранения
    # полученных данных.
    with open(filename, "wb") as f:
        received_bytes = 0
        # Получаем данные из файла блоками по
        # 1024 байта до объема
        # общего количество байт, сообщенных клиентом.
        while received_bytes < filesize:
            chunk = sck.recv(1024)
            if chunk:
                f.write(chunk)
                received_bytes += len(chunk)

def send_file(sck: socket.socket, filename):
    # Получение размера файла.
    filesize = os.path.getsize(filename)
    # В первую очередь сообщим серверу, 
    # сколько байт будет отправлено.
    sck.sendall(struct.pack("<Q", filesize))
    # Отправка файла блоками по 1024 байта.
    with open(filename, "rb") as f:
        while read_bytes := f.read(1024):
            sck.sendall(read_bytes)

def test_con():
    with socket.create_connection((ip_addr, port)) as conn:
        print("Подключение к серверу.")
        print("Передача файла...")
        send_file(conn, "image.png")
        print("Отправлено.")
    print("Соединение закрыто.")

#Ваша роль - high_admin. Выберите опцию...
def get_my_role(data):
    roles = ['high_admin', 'low_admin', 'top', 'trust', 'untrust', 'user']
    for x in roles:
        if x in data:
            return x

def main():
    #f = open('')
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        client_socket.connect((ip_addr, int(port)))
    except:
        print("ERROR! Не удаётся подключиться к серверу!")
        return
    role = 'user'
    message = ((client_socket.recv(1024)).decode('utf-8'))
    print(message)
    answer = input('>>')
    client_socket.send(answer.encode('utf-8'))
    message = ((client_socket.recv(1024)).decode('utf-8'))
    if message == 'Пожалуйста, введите данные для входа в аккаунт!':
        while True:
            answer = input('Login: ')
            client_socket.send(answer.encode('utf-8'))
            answer = input('Password: ')
            client_socket.send(answer.encode('utf-8'))
            message = ((client_socket.recv(1024)).decode('utf-8'))
            print(message)
            #message = ((client_socket.recv(1024)).decode('utf-8'))
            #print(message)
            #while True:
            if message == 'Вы были успешно авторизованы! Можете продолжать пользоваться сайтом':
                print("YES")
                message = ((client_socket.recv(1024)).decode('utf-8'))
                print(message)
                role = get_my_role(message)
                #message = ((client_socket.recv(1024)).decode('utf-8'))
                while True:
                    message = ((client_socket.recv(4096)).decode('utf-8'))
                    print(message)
                    answer = input('>>')
                    client_socket.send(answer.encode('utf-8'))
                    print(answer)
                    if role == 'high_admin':
                        if answer == '1':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            answer = input('>>')
                            client_socket.send(answer.encode('utf-8'))
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            if message == 'Данные пользователя успешно введены':
                                print(message)
                            else:
                                print(message)
                                print('Попробуйте ещё раз')
                        elif answer == '2':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            answer = input('>>')
                            client_socket.send(answer.encode('utf-8'))
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                        elif answer == '3':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            answer = input('>>')
                            client_socket.send(answer.encode('utf-8'))
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                        elif answer == '4':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                        elif answer == '5':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            print("Какой из файлов вы хотите отправить?(Введите название)")
                            for i in range(len(os.listdir())):
                                print(f'{i + 1}) {os.listdir()[i]}')
                            answer = input('>>')
                            #client_socket.send(answer.encode('utf-8'))
                            filename = answer
                            try:
                                FILE = open(filename, 'r')
                                FILE.close()
                                print('Введите уровень доступа статьи:')
                                answer = input('>>')
                                answer = filename + ' ' + answer
                                client_socket.send(answer.encode('utf-8'))
                                send_file(client_socket, filename)
                            except:
                                print("Не удалось найти этот файл!")
                                answer = 'Notfound'
                                client_socket.send(answer.encode('utf-8'))
                        elif answer == '6':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            answer = input('>>')
                            filename = answer.split(' ')[0]
                            #level = answer.split(' ')[1]
                            client_socket.send(answer.encode('utf-8'))
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            if message == 'Разрешён доступ':
                                receive_file(client_socket, filename)
                                print('Файл получен')
                            else:
                                print('Доступ запрещён либо файл не найден.')
                        elif answer == '7':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                        elif answer == '8':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            if message != 'Вы не можете редактировать!':
                                answer = input('>>')
                                filename = answer.split(' ')[0]
                                level = answer.split(' ')[1]
                                client_socket.send(answer.encode('utf-8'))
                                message = ((client_socket.recv(1024)).decode('utf-8'))
                                print(message)
                                if message == 'Разрешён доступ':
                                    receive_file(client_socket, filename)
                                    print('Файл получен')
                                    print('После того как отредактируете файл введите слово <continue>')
                                    go = ''
                                    while go != 'continue':
                                        go = input('>> ')
                                    print(filename)
                                    send_file(client_socket, filename)
                                else:
                                    print('Доступ запрещён либо файл не найден.')
                        elif answer == '9':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            answer = input('>>')
                            client_socket.send(answer.encode('utf-8'))
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print("Сообщение от сервера: ", message)
                            if message == 'OFF!!!':
                                print('Отсоединение!')
                                client_socket.close()
                                exit()
                            elif message == 'WORK!!!':
                                print('Продолжаем работу')
                        elif answer == '10':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            message = ((client_socket.recv(16384)).decode('utf-8'))
                            print(message)
                        elif answer == '11':
                            message = ((client_socket.recv(2048)).decode('utf-8'))
                            print(message)
                            message = ((client_socket.recv(16384)).decode('utf-8'))
                            print(message)
                        elif answer == '12':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                        elif answer == '13':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                        elif answer == '14':
                            client_socket.close()
                            exit()
                        else:
                            print('ERROR!')
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                    elif role == 'low_admin':
                        if answer == '1':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            answer = input('>>')
                            client_socket.send(answer.encode('utf-8'))
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            if message == 'Данные пользователя успешно введены':
                                print(message)
                            else:
                                print(message)
                                print('Попробуйте ещё раз')
                        elif answer == '2':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            answer = input('>>')
                            client_socket.send(answer.encode('utf-8'))
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                        elif answer == '3':
                            #добавить статью
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            print("Какой из файлов вы хотите отправить?(Введите название)")
                            for i in range(len(os.listdir())):
                                print(f'{i + 1}) {os.listdir()[i]}')
                            answer = input('>>')
                            #client_socket.send(answer.encode('utf-8'))
                            filename = answer
                            try:
                                FILE = open(filename, 'r')
                                FILE.close()
                                print('Введите уровень доступа статьи:')
                                answer = input('>>')
                                answer = filename + ' ' + answer
                                client_socket.send(answer.encode('utf-8'))
                                message = ((client_socket.recv(1024)).decode('utf-8'))
                                if message == 'Access':
                                    print('Файл отправлен')
                                    send_file(client_socket, filename)
                                else:
                                    print('Нет доступа для помещения файла на такой уровень кофиденциальности')
                            except:
                                print("Не удалось найти этот файл!")
                                answer = 'Notfound'
                                client_socket.send(answer.encode('utf-8'))
                        elif answer == '4':
                            #редактировать статью
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            if message != 'Вы не можете редактировать!':
                                answer = input('>>')
                                filename = answer.split(' ')[0]
                                level = answer.split(' ')[1]
                                client_socket.send(answer.encode('utf-8'))
                                message = ((client_socket.recv(1024)).decode('utf-8'))
                                print(message)
                                if message == 'Разрешён доступ':
                                    receive_file(client_socket, filename)
                                    print('Файл получен')
                                    print('После того как отредактируете файл введите слово <continue>')
                                    go = ''
                                    while go != 'continue':
                                        go = input('>> ')
                                    print(filename)
                                    send_file(client_socket, filename)
                                else:
                                    print('Доступ запрещён либо файл не найден.')
                        elif answer == '5':
                            #получить статью
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            answer = input('>>')
                            filename = answer.split(' ')[0]
                            #level = answer.split(' ')[1]
                            client_socket.send(answer.encode('utf-8'))
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            if message == 'Разрешён доступ':
                                receive_file(client_socket, filename)
                                print('Файл получен')
                            else:
                                print('Доступ запрещён либо файл не найден.')
                        elif answer == '6':
                            #получить список статей
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                        elif answer == '7':
                            client_socket.close()
                            exit()
                        else:
                            print('ERROR!')
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                    elif role == 'top' or role == 'trust' or role == 'untrust':
                        if answer == '1':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            print("Какой из файлов вы хотите отправить?(Введите название)")
                            for i in range(len(os.listdir())):
                                print(f'{i + 1}) {os.listdir()[i]}')
                            answer = input('>>')
                            #client_socket.send(answer.encode('utf-8'))
                            filename = answer
                            try:
                                FILE = open(filename, 'r')
                                FILE.close()
                                print('Введите уровень доступа статьи:')
                                answer = input('>>')
                                answer = filename + ' ' + answer
                                client_socket.send(answer.encode('utf-8'))
                                message = ((client_socket.recv(1024)).decode('utf-8'))
                                if message == 'Access':
                                    print('Файл отправлен')
                                    send_file(client_socket, filename)
                                else:
                                    print('Нет доступа для помещения файла на такой уровень кофиденциальности')
                            except:
                                print("Не удалось найти этот файл!")
                                answer = 'Notfound'
                                client_socket.send(answer.encode('utf-8'))
                        elif answer == '2':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            if message != 'Вы не можете редактировать!':
                                answer = input('>>')
                                filename = answer.split(' ')[0]
                                level = answer.split(' ')[1]
                                client_socket.send(answer.encode('utf-8'))
                                message = ((client_socket.recv(1024)).decode('utf-8'))
                                print(message)
                                if message == 'Разрешён доступ':
                                    receive_file(client_socket, filename)
                                    print('Файл получен')
                                    print('После того как отредактируете файл введите слово <continue>')
                                    go = ''
                                    while go != 'continue':
                                        go = input('>> ')
                                    print(filename)
                                    send_file(client_socket, filename)
                                else:
                                    print('Доступ запрещён либо файл не найден.')
                        elif answer == '3':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            answer = input('>>')
                            filename = answer.split(' ')[0]
                            #level = answer.split(' ')[1]
                            client_socket.send(answer.encode('utf-8'))
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            if message == 'Разрешён доступ':
                                receive_file(client_socket, filename)
                                print('Файл получен')
                            else:
                                print('Доступ запрещён либо файл не найден.')
                        elif answer == '4':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                        elif answer == '5':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                        elif answer == '6':
                            client_socket.close()
                            exit()
                        else:
                            print('ERROR!')
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                    else:
                        if answer == '1':
                            pass
                        elif answer == '2':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            answer = input('>>')
                            client_socket.send(answer.encode('utf-8'))
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            if message == 'Файл отправлен':
                                receive_file(client_socket, filename)
                        elif answer == '3':
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            print(message)
                            answer = input('>>')
                            client_socket.send(answer.encode('utf-8'))
                            message = ((client_socket.recv(1024)).decode('utf-8'))
                            if message == 'Ваши данные занесены в список на одобрение':
                                print(message)
                            else:
                                print(message)
                                print('Попробуйте ещё раз')
            else:
                if message.find('Вы поход забыли пароль! До свиданья!') != -1:
                    print('Вы поход забыли пароль! До свиданья!')
                    client_socket.close()
                    exit()
                    return
                else:
                    message = 'Пожалуйста, введите данные для входа в аккаунт!'
                    print(message)
    else:
        print('GERE')
        while True:
            '''while True:
            try:
                print('Я пытаюсь')
                message = ((client_socket.recv(16384)).decode('utf-8'))
                #print(message)
                break
            except:
                print('wait')
                time.sleep(1)'''
            message = ((client_socket.recv(16384)).decode('utf-8'))
            print(message)
            answer = input('>>')
            client_socket.send(answer.encode('utf-8'))
            if answer == '1':
                pass
            elif answer == '2':
                message = ((client_socket.recv(1024)).decode('utf-8'))
                print(message)
                answer = input('>>')
                print(answer)
                client_socket.send(answer.encode('utf-8'))
                message = ((client_socket.recv(1024)).decode('utf-8'))
                print(message)
                filename = answer
                if message == 'Файл отправлен':
                    receive_file(client_socket, filename)
            elif answer == '3':
                message = ((client_socket.recv(1024)).decode('utf-8'))
                print(message)
                answer = input('>>')
                client_socket.send(answer.encode('utf-8'))
                message = ((client_socket.recv(1024)).decode('utf-8'))
                if message == 'Ваши данные занесены в список на одобрение':
                    print(message)
                else:
                    print(message)
                    print('Попробуйте ещё раз')
            elif answer == '4':
                message = ((client_socket.recv(1024)).decode('utf-8'))
                print(message)
            elif answer == '5':
                break
            else:
                message_to_client = f'Вы ввели некорректный номер. Попробуйте ещё раз.'
                conn.send(message_to_client.encode('utf-8'))

    print('STOP')
    '''login = input("Введите логин: ")
    client_socket.sendall(login.encode())
    resp = ((client_socket.recv(1024)).decode())'''

if __name__ == "__main__":
    main()
    #test_con()